# POSIX-ish Makefile with extensions common to *BSD and GNU such as:
# - Usage of backticks for shell evaluation
# - Usage of ?= for defining variables when not already defined
# - Usage of += for appending to a variable

include config.mk

SRCS  = bookmarks.c fmt.c fmt_test.c uri.c uri_test.c keybindings.c downloads.c badwolf.c
OBJS  = bookmarks.o fmt.o uri.o keybindings.o downloads.o badwolf.o
OBJS_test = fmt_test.o uri_test.o bookmarks_test.o
EXE   = badwolf
EXE_test = fmt_test uri_test bookmarks_test
TRANS = fr.mo pt_BR.mo tr.mo de.mo vi.mo
TRANS_MAN = badwolf.fr.1
DOCS  = usr.bin.badwolf README.md KnowledgeBase.md interface.txt

GETTEXT_OPTS = --copyright-holder="Badwolf Authors <https://hacktivis.me/projects/badwolf>" --package-name="$(PACKAGE)" --package-version="$(VERSION_FULL)" --msgid-bugs-address="contact+badwolf-msgid@hacktivis.me"

all: config.mk $(EXE) $(TRANS) po/messages.pot $(TRANS_MAN) po/manpage.pot

config.mk: configure
	@echo "Error: You need to execute ./configure before running make"
	@exit 1

icons: $(ICON_SIZES)

icons/hicolor/scalable/apps/badwolf.svg: badwolf.svg
	mkdir -p icons/hicolor/scalable/apps
	scour --no-line-breaks --enable-id-stripping --remove-metadata $< $@

icons/hicolor/%/apps/badwolf.png: icons/hicolor/scalable/apps/badwolf.svg
	mkdir -p `dirname $@`
	$(INKSCAPE) `echo $@ | cut -d/ -f3 | ./icons_size.sh` $< -o $@

po/messages.pot: $(SRCS)
	xgettext --keyword=_ --language=C --from-code=UTF-8 -o $@ --add-comments --sort-output $(GETTEXT_OPTS) $(SRCS)

po/manpage.pot: badwolf.1
	po4a-gettextize --format man -M utf-8 --master badwolf.1 $(GETTEXT_OPTS) --po $@

po/%_man.po: po/manpage.pot badwolf.1
	po4a-updatepo --format man -M utf-8 --master badwolf.1 --msgmerge-opt '--update' $(GETTEXT_OPTS) --po $@

po/%.po: po/messages.pot
	msgmerge --update --backup=off $@ $<

${TRANS}: po/${@:.mo=.po}
	mkdir -p locale/${@:.mo=}/LC_MESSAGES
	$(MSGFMT) -o locale/${@:.mo=}/LC_MESSAGES/$(PACKAGE).mo po/${@:.mo=.po}

badwolf.fr.1: po/fr_man.po badwolf.1
	po4a-translate --format man -M utf-8 --master badwolf.1 --po po/fr_man.po --localized $@

badwolf: $(OBJS)
	$(CC) -std=c11 -o $@ $(OBJS) $(LDFLAGS) $(LIBS)

.c:
	$(CC) -std=c11 $(CFLAGS) $(LDFLAGS) $(LIBS) -o $@ $<

.c.o:
	$(CC) -std=c11 $(CFLAGS) -c -o $@ $<

uri_test: uri.o uri_test.o
	$(CC) -std=c11 -o $@ uri.o uri_test.o $(LDFLAGS) $(LIBS)

fmt_test: fmt.o fmt_test.o
	$(CC) -std=c11 -o $@ fmt.o fmt_test.o $(LDFLAGS) $(LIBS)

bookmarks_test: bookmarks.o bookmarks_test.o
	$(CC) -std=c11 -o $@ bookmarks.o bookmarks_test.o $(LDFLAGS) $(LIBS)


.PHONY: test
test: $(EXE_test)
	$(DBG) ./uri_test
	$(DBG) ./fmt_test
	$(DBG) ./bookmarks_test

.PHONY: lint
lint:
	$(MANDOC) -Tlint -Wunsupp,error,warning ./badwolf.1 $(TRANS_MAN)
	$(SHELLCHECK) ./configure
	$(FLAWFINDER) .

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -p badwolf $(DESTDIR)$(BINDIR)/
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -p badwolf.1 $(DESTDIR)$(MANDIR)/man1/
	# TODO: use $TRANS_MAN
	mkdir -p $(DESTDIR)$(MANDIR)/fr/man1
	cp -p badwolf.fr.1 $(DESTDIR)$(MANDIR)/fr/man1/badwolf.1
	mkdir -p $(DESTDIR)$(DATADIR)/locale
	cp -r locale/ $(DESTDIR)$(DATADIR)/
	cp interface.css $(DESTDIR)$(DATADIR)/
	mkdir -p $(DESTDIR)$(APPSDIR)
	cp -p badwolf.desktop $(DESTDIR)$(APPSDIR)/
	mkdir -p $(DESTDIR)$(DOCDIR)
	cp -p $(DOCS) $(DESTDIR)$(DOCDIR)/
	mkdir -p $(DESTDIR)$(PREFIX)/share
	cp -r icons $(DESTDIR)$(PREFIX)/share/
	@printf '\nNote: An example AppArmor profile has been installed at '$(DOCDIR)/usr.bin.badwolf'\n'

.PHONY: clean
clean:
	rm -fr locale $(OBJS) $(OBJS_test) $(EXE) $(EXE_test)

.PHONY: distclean
distclean: clean
	rm -fr config.mk

format: *.c *.h
	clang-format -style=file -assume-filename=.clang-format -i *.c *.h
