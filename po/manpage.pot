# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Badwolf Authors <https://hacktivis.me/projects/badwolf>
# This file is distributed under the same license as the Badwolf package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Badwolf 1.2.0+g815f5245.develop\n"
"Report-Msgid-Bugs-To: contact+badwolf-msgid@hacktivis.me\n"
"POT-Creation-Date: 2022-04-07 07:48+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Dd
#: badwolf.1:4
#, no-wrap
msgid "2022-04-06"
msgstr ""

#. type: Dt
#: badwolf.1:5
#, no-wrap
msgid "BADWOLF 1"
msgstr ""

#. type: Sh
#: badwolf.1:7
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: badwolf.1:9
msgid "E<.Nm badwolf>"
msgstr ""

#. type: Nd
#: badwolf.1:9
#, no-wrap
msgid "minimalist and privacy-oriented web browser based on WebKitGTK"
msgstr ""

#. type: Sh
#: badwolf.1:10
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: badwolf.1:14
msgid "E<.Nm> E<.Op Ar webkit/gtk options> E<.Op Ar URLs or paths>"
msgstr ""

#. type: Sh
#: badwolf.1:14
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: badwolf.1:17
msgid ""
"E<.Nm> is a minimalist browser that cares about privacy, it is based on "
"WebKitGTK and thus also accepts WebKitGTK (and dependencies) flags and "
"environment variables, unfortunately there doesn't seems to be manpages for "
"theses."
msgstr ""

#. type: Plain text
#: badwolf.1:21
msgid ""
"Runtime configuration specific to E<.Nm> will probably get added at a later "
"release."
msgstr ""

#. type: Sh
#: badwolf.1:21
#, no-wrap
msgid "KEYBINDINGS"
msgstr ""

#. type: Plain text
#: badwolf.1:25
msgid ""
"The following section lists the keybinding by their action, each item is "
"described by the widget the focus is on or E<.Aq any> if it works for the "
"whole window, followed by the keybind it grabs."
msgstr ""

#. type: It
#: badwolf.1:26
#, no-wrap
msgid "webview Ctrl-Scroll"
msgstr ""

#. type: Plain text
#: badwolf.1:28
msgid "Zooms the webpage in/out."
msgstr ""

#. type: It
#: badwolf.1:28
#, no-wrap
msgid "webview Ctrl-0"
msgstr ""

#. type: Plain text
#: badwolf.1:30
msgid "Resets webpage zoom to 100%."
msgstr ""

#. type: It
#: badwolf.1:30
#, no-wrap
msgid "any Ctrl-t"
msgstr ""

#. type: Plain text
#: badwolf.1:32
msgid "Creates a new tab (in a new session, similar as pressing the button)"
msgstr ""

#. type: It
#: badwolf.1:32
#, no-wrap
msgid "browser Ctrl-F4, browser Alt-d"
msgstr ""

#. type: Plain text
#: badwolf.1:34
msgid "Closes the current tab"
msgstr ""

#. type: It
#: badwolf.1:34
#, no-wrap
msgid "browser Ctrl-f"
msgstr ""

#. type: Plain text
#: badwolf.1:36
msgid "Focuses on the search entry"
msgstr ""

#. type: It
#: badwolf.1:36
#, no-wrap
msgid "browser Ctrl-l"
msgstr ""

#. type: Plain text
#: badwolf.1:38
msgid "Focuses on the location(URL) entry"
msgstr ""

#. type: It
#: badwolf.1:38
#, no-wrap
msgid "browser Ctrl-Shift-r / Ctrl-r, browser F5"
msgstr ""

#. type: Plain text
#: badwolf.1:40
msgid "Reloads the content in the current tab (with/without clearing cache)"
msgstr ""

#. type: It
#: badwolf.1:40
#, no-wrap
msgid "browser Escape"
msgstr ""

#. type: Plain text
#: badwolf.1:42
msgid "Stops loading the content in the current tab"
msgstr ""

#. type: It
#: badwolf.1:42
#, no-wrap
msgid "browser F7"
msgstr ""

#. type: Plain text
#: badwolf.1:44
msgid "Toggles caret browsing."
msgstr ""

#. type: It
#: badwolf.1:44
#, no-wrap
msgid "browser F12"
msgstr ""

#. type: Plain text
#: badwolf.1:46
msgid "Opens the web inspector."
msgstr ""

#. type: It
#: badwolf.1:46
#, no-wrap
msgid "browser Ctrl-[ / Ctrl-]"
msgstr ""

#. type: Plain text
#: badwolf.1:48
msgid "Go back/forward in current tab's history"
msgstr ""

#. type: It
#: badwolf.1:48
#, no-wrap
msgid "browser Ctrl-p"
msgstr ""

#. type: Plain text
#: badwolf.1:50
msgid "Print the current page. (spawns a dialog)"
msgstr ""

#. type: It
#: badwolf.1:50
#, no-wrap
msgid "any Alt-Left / Alt-Right"
msgstr ""

#. type: Plain text
#: badwolf.1:52 badwolf.1:63
msgid "Go to the previous/next tab"
msgstr ""

#. type: It
#: badwolf.1:52
#, no-wrap
msgid "any F1"
msgstr ""

#. type: Plain text
#: badwolf.1:54
msgid "Shows the about dialog"
msgstr ""

#. type: It
#: badwolf.1:54
#, no-wrap
msgid "any Alt-n"
msgstr ""

#. type: Plain text
#: badwolf.1:57
msgid "Where n is any numeric-row key.  Go to the n-th tab, 0 goes to the last one."
msgstr ""

#. type: Ss
#: badwolf.1:58
#, no-wrap
msgid "DEFAULT ONES"
msgstr ""

#. type: Plain text
#: badwolf.1:60
msgid "Here is a incomplete list of the default Webkit/GTK keybindings:"
msgstr ""

#. type: It
#: badwolf.1:61
#, no-wrap
msgid "any Ctrl-PageUp / Ctrl-PageDown"
msgstr ""

#. type: It
#: badwolf.1:63
#, no-wrap
msgid "search Ctrl-g / Ctrl-Shift-g"
msgstr ""

#. type: Plain text
#: badwolf.1:65
msgid "When the search box is focused it goes to the Next/Previous search term."
msgstr ""

#. type: It
#: badwolf.1:65
#, no-wrap
msgid "search Escape"
msgstr ""

#. type: Plain text
#: badwolf.1:67
msgid "Cancels current search"
msgstr ""

#. type: Sh
#: badwolf.1:68
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: It
#: badwolf.1:70
#, no-wrap
msgid "Ev BADWOLF_L10N"
msgstr ""

#. type: Plain text
#: badwolf.1:76
msgid ""
"A colon-separated list in the form lang_COUNTRY where lang is in ISO-639 and "
"COUNTRY in ISO-3166.  For example E<.Ic "
"BADWOLF_L10N=\"en_GB:fr_FR:de_DE\">.  When this variable isn't set, spelling "
"isn't activated.  A more generic variable name is also intended to be used "
"in the future."
msgstr ""

#. type: Plain text
#: badwolf.1:81
msgid ""
"To get the list of supported dictionaries execute E<.Ic enchant-lsmod-2 "
"-list-dicts> or before enchant 2.0: E<.Ic enchant-lsmod -list-dicts>"
msgstr ""

#. type: Sh
#: badwolf.1:82
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: badwolf.1:88
msgid ""
"The following paths are using E<.Xr sh 1> syntax to correctly support XDG "
"Base Directory Specification, you can use the E<.Xr echo 1> command to check "
"where it is on your system."
msgstr ""

#. type: It
#: badwolf.1:90
#, no-wrap
msgid "Pa ${XDG_CONFIG_HOME:-$HOME/.config}/badwolf/content-filters.json"
msgstr ""

#. type: Plain text
#: badwolf.1:93
msgid ""
"WebKit-specific content-filter file, this allows to block unwanted content "
"(ads, nagware, ...).  For some introductory information about the format "
"see:"
msgstr ""

#. type: Plain text
#: badwolf.1:98
msgid "For a converter using AblockPlus-style filters, try:"
msgstr ""

#. type: Plain text
#: badwolf.1:101
msgid "For a ready-to-use file (that you should update periodically), try:"
msgstr ""

#. type: It
#: badwolf.1:102
#, no-wrap
msgid "Pa ${XDG_CACHE_HOME:-$HOME/.cache}/badwolf/filters"
msgstr ""

#. type: Plain text
#: badwolf.1:105
msgid ""
"This is where the compiled filters are stored, the file(s) in it are "
"automatically generated and so shouldn't be edited.  Documented here only "
"for sandboxing / access-control purposes."
msgstr ""

#. type: It
#: badwolf.1:105
#, no-wrap
msgid "Pa ${XDG_DATA_HOME:-$HOME/.local/share}/badwolf/bookmarks.xbel"
msgstr ""

#. type: Plain text
#: badwolf.1:110
msgid ""
"XBEL (XML Bookmark Exchange Language) file, known to be currently supported "
"by: E<.Xr elinks 1>, E<.Xr konqueror 1>, E<.Xr kbookmarkeditor 1 . Doing a "
"symbolic link from their path works fine but you might also want to use "
"XInclude to merge multiple XBEL files.>"
msgstr ""

#. type: Plain text
#: badwolf.1:112
msgid "For more information about this format see:"
msgstr ""

#. type: Plain text
#: badwolf.1:115
msgid "For an example XBEL file see:"
msgstr ""

#. type: It
#: badwolf.1:116
#, no-wrap
msgid "Pa ${XDG_DATA_HOME:-$HOME/.local/share}/badwolf/webkit-web-extension/"
msgstr ""

#. type: Plain text
#: badwolf.1:118
msgid "Directory containing the"
msgstr ""

#. type: Plain text
#: badwolf.1:121
msgid ""
"to be loaded into E<.Nm . Note: They aren't the JavaScript-based "
"Web-Extensions supported by Firefox or Chrome, but native code in shared "
"objects using the WebKitGTK API.>"
msgstr ""

#. type: Plain text
#: badwolf.1:123
msgid "Examples of useful extensions may be found at:"
msgstr ""

#. type: It
#: badwolf.1:124
#, no-wrap
msgid "Pa ${DATADIR:-/usr/local/share}/badwolf/interface.css"
msgstr ""

#. type: It
#: badwolf.1:125
#, no-wrap
msgid "Pa ${XDG_DATA_HOME:-$HOME/.local/share}/badwolf/interface.css"
msgstr ""

#.  GNOME can't even into doing any redirections of the few docs available
#. type: Plain text
#: badwolf.1:131
msgid ""
"CSS files (respectively system and user-level) for styling E<.Nm> "
"interface.  See"
msgstr ""

#. type: Plain text
#: badwolf.1:133
msgid "for the properties being available."
msgstr ""

#. type: Plain text
#: badwolf.1:139
msgid ""
"For testing your styles I would recommend using the E<.Ev "
"GTK_DEBUG=interactive> environment variable on launching E<.Nm> and going to "
"the CSS tab."
msgstr ""

#. type: Sh
#: badwolf.1:140
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: badwolf.1:142
msgid "E<.An Haelwenn (lanodan) Monnier Aq Mt contact+badwolf@hacktivis.me>"
msgstr ""

#. type: Sh
#: badwolf.1:142
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: badwolf.1:144
msgid "You can submit contributions or tickets to"
msgstr ""

#. type: Plain text
#: badwolf.1:147
msgid "with E<.Xr git-send-email 1> for patches."
msgstr ""
